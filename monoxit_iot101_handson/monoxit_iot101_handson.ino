/* monoxit_iot101_handson.ino
 * This scketch is based on mqtt_esp8266.ino included with the pubsubclient by Nick O'Leary
 * pubsubclientライブラリに添付のmqtt_esp8266.ino by Nick O'Learyに基づく
 *
 * This software is released under the MIT License.
 * http://opensource.org/licenses/mit-license.php
*/

// ESP8266 WiFi ライブラリとPubSubClient(MQTTクライアント）ライブラリを使うと指定
// ライブラリ：ある機能を容易に使うことができるようにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ネットワーク環境に合わせて修正
const char* ssid = "YOURSSID";
const char* password = "WIFIPSWD";

// MQTTデバイスIDのプレフィックス
// プレフィックスの後にESP8266のデバイスID(シリアル番号)が付加されMQTTのデバイスIDが作られる
// 「test」としたときは「test30f3ab」のようなIDとなる
const char* mqttDeviceIdPrefix = "";

const char* topicPrefix = "mxitiot/";

// テスト用途の公開MQTTブローカ
//const char* mqttServer = "test.mosquitto.org";
const char* mqttServer = "broker.hivemq.com";
//const char* mqttServer = "iot.eclipse.org";
//const char* mqttServer = "192.168.133.206"; //セミナ会場バックアップ

// GPIO端子の指定
const int ledPin = 2;
const int irmotionPin = 14;

String mqttDeviceId;
String stateTopic;
String irmotionTopic;
String ledTopic;

//最後にMQTTで送信（パブリッシュ）した人感センサ値を保持する変数をLOWで初期化する
int lastPublishedIrState = LOW;

WiFiClient espClient;
// MQTTクライアントライブラリを「client」という名前で使えるようにする
PubSubClient client(espClient);

void setup() {
  WiFi.mode(WIFI_STA); // WiFi機能を子機専用モードに切り替える
  pinMode(ledPin, OUTPUT);     // LEDピンを出力モードにする
  Serial.begin(115200); // パソコンとつないだUSB通信速度を設定する

  // WiFiにつなぐ部分を呼び出してWiFiにつないだ状態にする
  setupWifi();

  // ESP8266のチップID（シリアル番号）を取得し16進数で変数chipIdにセットする
  String chipId = String(ESP.getChipId(),HEX);
  // あらかじめ設定したプレフィックスとチップIDでMQTTクライアントIDを作る
  mqttDeviceId = String(mqttDeviceIdPrefix) + chipId;
  Serial.print("mqttDeviceId:");
  Serial.println(mqttDeviceId); // [動作確認用]IDをUSB経由でパソコンに送信する
  
  // デバイスIDを組み合わせたMQTTトピック名を作り各変数へ保存しておく
  // トピック名はprefix/1a2b3c/stateといった名前となる
  stateTopic = topicPrefix + chipId + "/state";
  irmotionTopic = topicPrefix + chipId + "/irmotion";
  ledTopic = topicPrefix + chipId + "/led";
  Serial.print("IR Topic:");
  Serial.println(irmotionTopic); // [動作確認用]IDをUSB経由でパソコンに送信する
  Serial.print("LED Topic:");
  Serial.println(ledTopic); // [動作確認用]IDをUSB経由でパソコンに送信する
  // MQTTクライアントに、接続先アドレスとポート番号を設定
  client.setServer(mqttServer, 1883);
  // MQTTクライアントに、サーバーからメッセージを受信したときに実行する部分を指定
  client.setCallback(callback);
}

// WiFiにつなぐ部分
// プログラムの他の部分にsetupWifi()があると実際はここの{から}が実行される
void setupWifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTTサーバーからメッセージを受信したときに実行させる部分
void callback(char* topic, byte* payload, unsigned int length) {

  // Serial.で始まる命令は動作確認用にプログラマが入れた命令　()内がUSB経由パソコンへ送信される
  // パソコン側Arduino IDEのシリアルモニタ機能で送信された内容を表示することで、プログラムが正常に動作しているかどうかがわかる
  Serial.print("Message arrived [");  // [動作確認用]トピック名をUSB経由でパソコンに送信
  Serial.print(topic);
  Serial.print("] ");
  
  // [動作確認用]受信したMQTTメッセージのペイロードをUSB経由でパソコンに送信
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // ペイロードの先頭文字が「1」のときLEDをONにする
  if ((char)payload[0] == '1') {
    digitalWrite(ledPin, HIGH);
  } else { //でなければ
    digitalWrite(ledPin, LOW);// LEDをOFFにする
  }

}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイスIDを指定してMQTTブローカに接続開始
    if (client.connect(mqttDeviceId.c_str())) {
      Serial.println("connected");
      // 接続できたらトピック/stateに「mqttDeviceId connected」をパブリッシュ
	    String mqttPayload = mqttDeviceId + " connected";
      client.publish(stateTopic.c_str(), mqttPayload.c_str());
      // さらにトピック/ledに申し込み
      client.subscribe(ledTopic.c_str());
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // 接続できないときは5秒待つ
      delay(5000);
    }
  }
}

// 永久に繰り返す部分
void loop() {
  // MQTTサーバーに接続していなかったらreconnect()の部分を実行
  if (!client.connected()) {
    reconnect();
  }
  client.loop(); // pubsubclient使用時にはloop内にこの命令を入れる

  // 人感センサの状態(HIGH/LOW)を読み取る irState変数にHIGH(1)かLOW(0)がセットされる
  int irState = digitalRead(irmotionPin);
  
  // LOWからHIGHに変化したときにON, HIGHからLOWに変化したときにOFFを送信（パブリッシュ）する
  // もし irStateがHIGHでかつ最後にパブリッシュした状態がLOWならば ONをパブリッシュ
  if(irState == HIGH && lastPublishedIrState == LOW){
    lastPublishedIrState = HIGH;
    client.publish(irmotionTopic.c_str(), "ON"); // ONをパブリッシュ
    Serial.print("Publish:");
    Serial.print(irmotionTopic);
    Serial.print(":");
    Serial.println("ON");
  // もし irStateがLOWでかつ最後にパブリッシュした状態がHIGHならばOFFをパブリッシュ
  }else if(irState == LOW && lastPublishedIrState == HIGH){
    lastPublishedIrState = LOW;
    client.publish(irmotionTopic.c_str(), "OFF");	// OFFをパブリッシュ
    Serial.print("Publish:");
    Serial.print(irmotionTopic);
    Serial.print(":");
    Serial.println("OFF");
  }
}
